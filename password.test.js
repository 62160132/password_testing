const { checkLength, checkAlphabet, checkDigit, checkSymbol, checkPassword } = require('./password')
describe('Test Password Length', () => {
  test('should 8 characters to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })

  test('should 7 characters to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })

  test('should 25 characters to be true', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })

  test('should 26 characters to be false', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })
})

describe('Test Alphabet', () => {
  test('should has alphabet m in password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })

  test('should has alphabet A in password', () => {
    expect(checkAlphabet('A')).toBe(true)
  })

  test('should has not alphabet in password', () => {
    expect(checkAlphabet('11111')).toBe(false)
  })
})

describe('Test Digit', () => {
  test('should has digit in password', () => {
    expect(checkDigit('1')).toBe(true)
  })

  test('should has not digit in password', () => {
    expect(checkDigit('a')).toBe(false)
  })
})

describe('Test Symbol', () => {
  test('should has symbol in password', () => {
    expect(checkSymbol('111!111')).toBe(true)
  })

  test('should has not symbol in password', () => {
    expect(checkSymbol('111111')).toBe(false)
  })
})

describe('Test Password', () => {
  test('should password xxxxxxx to be false', () => {
    expect(checkPassword('test@11')).toBe(false)
  })

  test('should password xxxxxxx to be true', () => {
    expect(checkPassword('test#111')).toBe(true)
  })
})
